#pragma once
#include <fstream>
#include <string>
#include <thread>
#include <iostream>
#include <mutex>


void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);
