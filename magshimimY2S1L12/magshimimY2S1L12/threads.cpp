#include "threads.h"
#include <vector>
std::mutex myMutex;

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	std::string a = " ";
	bool isPrime = true;
	for (int n = begin; n < end; n++)
	{
		for (int i = 2; i <= n / 2; ++i)
		{
			if (n % i == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			std::lock_guard<std::mutex> myLock(myMutex);
			a += std::to_string(n);
			file << a;
		}
		isPrime = true;
	}
	file << std::endl;
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream myFile;
	myFile.open(filePath);
	std::vector<std::thread*> myThreads;
	if (!myFile.is_open()) {
		std::cout << "Error opening the file!" << std::endl;
		return;
	}
	int jump = (end - begin) / N, j = 0;
	for (int i = begin; i < end - jump; i += jump, j++)
	{
		myThreads.push_back(new std::thread(writePrimesToFile, i, i + jump, std::ref(myFile)));
	}
	clock_t t;
	t = clock();
	for (int i = 0; i < N - 1; i++)
	{
		myThreads[i]->join();
	}
	t = clock() - t;
	double time_taken = ((double)t) / CLOCKS_PER_SEC;
	std::cout << "writePrimesToFile took " << time_taken << " to execute" << std::endl;
	myFile.close();
}
