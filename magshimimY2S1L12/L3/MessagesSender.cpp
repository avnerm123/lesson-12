#include "MessagesSender.h"

mutex m2;
mutex m1;
condition_variable condVar;



bool MessagesSender::searchUser(string user)
{
	for (int i = 0; i < this->Users.size(); i++)
	{
		if (this->Users[i] == user)
			return true;
	}
	return false;
}



void MessagesSender::Signin()
{
	string userName;
	LOGIN;
	cin >> userName;
	while (this->searchUser(userName))
	{
		USER_EXISTED;
		cin >> userName;
	}
	this->Users.push_back(userName);
}


void MessagesSender::Signout()
{
	string userName;
	SIGNOUT;
	cin >> userName;
	while (this->searchUser(userName))
	{
		USER_DONT_EXIST;
		cin >> userName;
	}
	for (int i = 0; i < this->Users.size(); i++)
	{
		if (this->Users[i] == userName)
			this->Users.erase(this->Users.begin() + i);
	}
}


void MessagesSender::Connected_Users()
{
	for (int i = 0; i < this->Users.size(); i++)
	{
		USER_ON;
	}
}

void MessagesSender::readFileThreadFunc()
{
	
	string line;
	int i = 0;
	while (true)
	{
		ifstream FileToRead(INFILE);
		if (FileToRead.is_open())
		{
			std::unique_lock<std::mutex> lck(m1);
			//lck.lock();
			while (getline(FileToRead, line))
			{
				if (i == this->Users.size())
					i = 0;				
				this->msgQueue.push(pair<string, string>(this->Users[i], line));
				i++;\
			}
			//lck.unlock();
			condVar.notify_all();
		}
		FileToRead.close();
		SLEEP;
	}
}


void MessagesSender::sendmsgQueue()
{
	while (true)
	{
		std::unique_lock<std::mutex> lck(m1);
		condVar.wait(lck);
		ofstream FileToWrite;
		FileToWrite.open("output.txt");
		
		if (FileToWrite.is_open()) {

			cout << "BAZINGA!";
		}
		while (!this->msgQueue.empty())
		{
			pair<string, string> current = this->msgQueue.front();
			this->msgQueue.pop();
			FileToWrite << current.first << ":" << current.second << std::endl;
		}
		
		FileToWrite.close();
	}
}



