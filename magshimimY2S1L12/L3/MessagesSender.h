#pragma once

#include <iostream>
#include <vector>
#include <chrono>
#include <string>
#include <thread>
#include <fstream>
#include <queue>
#include <condition_variable>
#include <mutex>

#define MENU cout << "1. Signin \n2. Signout\n3. Connected Users\n4. exit\n"
#define LOGIN cout << "Enter a new userName: "
#define SIGNOUT cout << "Enter your userName to sign out: "
#define USER_EXISTED cout << "The name you've Entered already exist please try again: "
#define USER_DONT_EXIST cout << "The name you've Entered dos'nt exist please try again : "
#define USER_ON cout << this->Users[i] << " is Now on!\n";
#define SLEEP  std::this_thread::sleep_for(std::chrono::seconds(60))
#define INFILE "data.txt"
#define OUTFILE "output.txt"
//vars
using std::vector;
using std::string;
using std::queue;
using std::pair;
using std::ifstream;
using std::ofstream;
using std::mutex;
using std::lock_guard;
using std::condition_variable;
//in,out
using std::cin;
using std::cout;
//func's
using std::getline;



class MessagesSender{

	vector<string> Users;
	queue<pair<string, string>> msgQueue; //queue of a pairs: key = userName, val = msg;

	bool searchUser(string user);
public:
	void Signin();
	void Signout();
	void Connected_Users();
	void readFileThreadFunc();
	void sendmsgQueue();
};

