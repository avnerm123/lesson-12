#include "MessagesSender.h"

#define SINGIN 1
#define SIGNOUT 2
#define CONNECTED_USERS 3
#define EXIT 4

int main()
{
	int choice = 0;
	MessagesSender myMsg;
	
	

	while (choice != EXIT)
	{
		MENU;
		cin >> choice;
		switch (choice)
		{
		case SINGIN:
		{
			myMsg.Signin();
			break;
		}
		case SIGNOUT:
		{
			myMsg.Signout();
			break;
		}
		case CONNECTED_USERS:
		{
			myMsg.Connected_Users();
			break;
		}
		default:
			break;
		}
	}
	std::thread q2(&MessagesSender::readFileThreadFunc, std::ref(myMsg));
	std::thread q1(&MessagesSender::sendmsgQueue, std::ref(myMsg));



	q1.detach();
	q2.detach();
	std::this_thread::sleep_for(std::chrono::seconds(2));
	return 0;
}